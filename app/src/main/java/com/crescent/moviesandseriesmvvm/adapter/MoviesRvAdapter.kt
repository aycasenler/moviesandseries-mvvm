package com.crescent.moviesandseriesmvvm.adapter

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.crescent.moviesandseriesmvvm.R
import com.crescent.moviesandseriesmvvm.databinding.MoviesRecyclerviewBinding
import com.crescent.moviesandseriesmvvm.model.MoviesProperties

class MoviesRvAdapter( private var moviesList: List<MoviesProperties>) : RecyclerView.Adapter<MoviesRvAdapter.MoviesViewHolder>() {

    override fun getItemCount(): Int {
        return moviesList.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MoviesViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.movies_recyclerview,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {

        var imageUrl = moviesList[position].poster_path

        if (!imageUrl.contains("https://image.tmdb.org/t/p/w500")){
            imageUrl = "https://image.tmdb.org/t/p/w500" + moviesList[position].poster_path
            moviesList[position].poster_path = imageUrl
        }

        holder.recyclerviewMovieBinding.movie = moviesList[position]

        holder.recyclerviewMovieBinding.movieLl.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt("movieId", moviesList[position].id)
            Navigation.findNavController(it).navigate(R.id.movieDetailFragment,bundle)
        }


    }



    inner class MoviesViewHolder(
        val recyclerviewMovieBinding: MoviesRecyclerviewBinding
    ) : RecyclerView.ViewHolder(recyclerviewMovieBinding.root)


}