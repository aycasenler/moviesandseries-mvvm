package com.crescent.moviesandseriesmvvm.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.crescent.moviesandseriesmvvm.R
import com.crescent.moviesandseriesmvvm.databinding.SeriesRecyclerviewBinding
import com.crescent.moviesandseriesmvvm.model.seriesModel.SeriesProperties

class SeriesRvAdapter(private var seriesList: List<SeriesProperties>) : RecyclerView.Adapter<SeriesRvAdapter.MoviesViewHolder>() {

    override fun getItemCount(): Int {
        return seriesList.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MoviesViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.series_recyclerview,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        var imageUrl = seriesList[position].poster_path

        if (!imageUrl.contains("https://image.tmdb.org/t/p/w500")){
            imageUrl = "https://image.tmdb.org/t/p/w500" + seriesList[position].poster_path
            seriesList[position].poster_path = imageUrl
        }

        holder.recyclerviewMovieBinding.series = seriesList[position]
        holder.recyclerviewMovieBinding.seriesLl.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt("seriesId", seriesList[position].id)
            Navigation.findNavController(it).navigate(R.id.seriesDetailFragment,bundle)
        }
    }

    inner class MoviesViewHolder(
        val recyclerviewMovieBinding: SeriesRecyclerviewBinding
    ) : RecyclerView.ViewHolder(recyclerviewMovieBinding.root)


}