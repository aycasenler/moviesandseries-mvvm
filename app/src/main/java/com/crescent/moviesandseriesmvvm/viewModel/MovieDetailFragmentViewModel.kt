package com.crescent.moviesandseriesmvvm.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.crescent.moviesandseriesmvvm.repository.MovieDetailFragmentRepository

class MovieDetailFragmentViewModel (application: Application) : AndroidViewModel(application) {
    private val repository = MovieDetailFragmentRepository(application)
    val showProgress: LiveData<Boolean>
    val movieTitle : LiveData<String>
    val movieOriginalTitle : LiveData<String>
    val movieOverview: LiveData<String>
    val movieReleaseDate: LiveData<String>
    val movieVoteAverage: LiveData<String>
    val moviePosterUrl: LiveData<String>
    val movieBackdropUrl: LiveData<String>
    val movieGenres: LiveData<String>
    val movieProductionCompanies: LiveData<String>

    init {
        showProgress = repository.showProgress
        movieTitle = repository.movieTitle
        movieOriginalTitle = repository.movieOriginalTitle
        movieOverview = repository.movieOverview
        movieReleaseDate = repository.movieReleaseDate
        movieVoteAverage = repository.movieVoteAverage
        moviePosterUrl = repository.moviePosterUrl
        movieBackdropUrl = repository.movieBackdropUrl
        movieGenres = repository.movieGenres
        movieProductionCompanies = repository.movieProductionCompanies
    }

    fun getMovieDetail(movieId : Int){
        repository.getMovieDetail(movieId)
    }


}