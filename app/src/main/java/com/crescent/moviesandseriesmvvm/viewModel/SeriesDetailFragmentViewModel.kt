package com.crescent.moviesandseriesmvvm.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.crescent.moviesandseriesmvvm.repository.SeriesDetailFragmentRepository

class SeriesDetailFragmentViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = SeriesDetailFragmentRepository(application)
    val showProgress: LiveData<Boolean>
    val seriesTitle : LiveData<String>
    val seriesOriginalTitle : LiveData<String>
    val seriesOverview: LiveData<String>
    val seriesFirstAirDate: LiveData<String>
    val seriesVoteAverage: LiveData<String>
    val seriesPosterUrl: LiveData<String>
    val seriesBackdropUrl: LiveData<String>
    val seriesGenres: LiveData<String>
    val seriesProductionCompanies: LiveData<String>
    val seriesSeasons: LiveData<String>
    val seriesEpisodes: LiveData<String>

    init {
        showProgress = repository.showProgress
        seriesTitle = repository.seriesTitle
        seriesOriginalTitle = repository.seriesOriginalTitle
        seriesOverview = repository.seriesOverview
        seriesFirstAirDate = repository.seriesFirstAirDate
        seriesVoteAverage = repository.seriesVoteAverage
        seriesPosterUrl = repository.seriesPosterUrl
        seriesBackdropUrl = repository.seriesBackdropUrl
        seriesGenres = repository.seriesGenres
        seriesProductionCompanies = repository.seriesProductionCompanies
        seriesSeasons = repository.seriesSeasons
        seriesEpisodes = repository.seriesEpisodes



    }


    fun getSeriesDetail(seriesId : Int){
        repository.getSeriesDetail(seriesId)
    }


}