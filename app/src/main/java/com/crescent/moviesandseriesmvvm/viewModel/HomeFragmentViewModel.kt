package com.crescent.moviesandseriesmvvm.viewModel

import android.app.Application
import android.content.Context
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.crescent.moviesandseriesmvvm.model.MoviesProperties
import com.crescent.moviesandseriesmvvm.model.seriesModel.SeriesProperties
import com.crescent.moviesandseriesmvvm.repository.HomeFragmentRepository
import com.crescent.moviesandseriesmvvm.view.HomeFragment
import kotlinx.android.synthetic.main.home_fragment.*
import kotlinx.android.synthetic.main.home_fragment.view.*
import net.cachapa.expandablelayout.ExpandableLayout

class HomeFragmentViewModel(application: Application) : AndroidViewModel(application) {

    private val repository = HomeFragmentRepository(application)
    val showProgress: LiveData<Boolean>
    val moviesList: LiveData<List<MoviesProperties>>
    val seriesList: LiveData<List<SeriesProperties>>
    val isExpanded = MutableLiveData<Boolean>()
    var isMovie = true
    var oldSize = 20

    init {
        showProgress = repository.showProgress
        moviesList = repository.moviesList
        seriesList = repository.seriesList

    }

    fun getMovies() {
        repository.getMovies(oldSize)

        isMovie = true

    }

    fun changeListSize(size: Int) {
        if(size != oldSize){
            oldSize = size
            if (isMovie)
                repository.getMovies(size)
            else
                repository.getSeries(size)
        }

        countBtnListener()
    }

    fun getSeries() {
        repository.getSeries(oldSize)

        isMovie = false
    }

    fun countBtnListener() {
        if (isExpanded.value == null)
            isExpanded.value = true
        else
            isExpanded.value = !isExpanded.value!!

    }
}