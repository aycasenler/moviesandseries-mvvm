package com.crescent.moviesandseriesmvvm.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.crescent.moviesandseriesmvvm.R
import com.crescent.moviesandseriesmvvm.databinding.SeriesDetailFragmentBinding
import com.crescent.moviesandseriesmvvm.viewModel.SeriesDetailFragmentViewModel
import kotlinx.android.synthetic.main.home_fragment.*

class SeriesDetailFragment : Fragment() {
    private lateinit var viewModel: SeriesDetailFragmentViewModel
    lateinit var binding: SeriesDetailFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(SeriesDetailFragmentViewModel::class.java)
        binding =
            DataBindingUtil.inflate(inflater, R.layout.series_detail_fragment, container, false)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel
        val view = binding.root

        viewModel.showProgress.observe(viewLifecycleOwner, Observer {
            if (it)
                search_progress.visibility = View.VISIBLE
            else
                search_progress.visibility = View.GONE
        })

        val seriesId = this.arguments?.getInt("seriesId")
        viewModel.getSeriesDetail(seriesId!!)

        return view
    }
}