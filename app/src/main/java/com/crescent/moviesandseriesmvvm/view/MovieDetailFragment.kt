package com.crescent.moviesandseriesmvvm.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.crescent.moviesandseriesmvvm.R
import com.crescent.moviesandseriesmvvm.databinding.MovieDetailFragmentBinding
import com.crescent.moviesandseriesmvvm.viewModel.MovieDetailFragmentViewModel
import kotlinx.android.synthetic.main.home_fragment.*

class MovieDetailFragment : Fragment() {
    private lateinit var viewModel: MovieDetailFragmentViewModel
    lateinit var binding: MovieDetailFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(MovieDetailFragmentViewModel::class.java)
        binding =
            DataBindingUtil.inflate(inflater, R.layout.movie_detail_fragment, container, false)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel
        val view = binding.root

        viewModel.showProgress.observe(viewLifecycleOwner, Observer {
            if (it)
                search_progress.visibility = View.VISIBLE
            else
                search_progress.visibility = View.GONE
        })

        val movieId = this.arguments?.getInt("movieId")
        viewModel.getMovieDetail(movieId!!)

        return view
    }
}