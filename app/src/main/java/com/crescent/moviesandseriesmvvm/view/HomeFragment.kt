package com.crescent.moviesandseriesmvvm.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.crescent.moviesandseriesmvvm.R
import com.crescent.moviesandseriesmvvm.adapter.MoviesRvAdapter
import com.crescent.moviesandseriesmvvm.adapter.SeriesRvAdapter
import com.crescent.moviesandseriesmvvm.databinding.HomeFragmentBinding
import com.crescent.moviesandseriesmvvm.viewModel.HomeFragmentViewModel
import kotlinx.android.synthetic.main.home_fragment.*
import kotlinx.android.synthetic.main.home_fragment.view.*
import net.cachapa.expandablelayout.ExpandableLayout

class HomeFragment : Fragment() {
    private lateinit var viewModel: HomeFragmentViewModel
    lateinit var binding: HomeFragmentBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModel = ViewModelProvider(this).get(HomeFragmentViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.home_fragment, container, false)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel

        val view = binding.root

        viewModel.getSeries()
        viewModel.showProgress.observe(viewLifecycleOwner, Observer {
            if (it)
                search_progress.visibility = VISIBLE
            else
                search_progress.visibility = GONE
        })
        viewModel.isExpanded.observe(viewLifecycleOwner, Observer {
            if (it)
                data_count_expandable_view.expand()
            else
                data_count_expandable_view.collapse()
        })

        viewModel.moviesList.observe(viewLifecycleOwner, Observer {
            view.list_movies_rv.adapter = MoviesRvAdapter(it)

            list_series_rv.visibility = GONE
            list_movies_rv.visibility = VISIBLE

        })

        viewModel.seriesList.observe(viewLifecycleOwner, Observer {
            list_series_rv.adapter = SeriesRvAdapter(it)

            list_series_rv.visibility = VISIBLE
            list_movies_rv.visibility = GONE
        })

        return view
    }


}