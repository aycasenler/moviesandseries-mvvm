package com.crescent.moviesandseriesmvvm.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.crescent.moviesandseriesmvvm.R
import com.rbddevs.splashy.Splashy

class SplashScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen_activity)

        Splashy(this)
            .setLogo(R.drawable.cinema)
            .setAnimation(Splashy.Animation.GLOW_LOGO)
            .setLogoWHinDp(100  ,100)
            .setFullScreen(true)
            .showTitle(false)
            .setTime(5000)
            .show()

        Splashy.onComplete(object : Splashy.OnComplete {
            override fun onComplete() {
                startActivity(Intent(this@SplashScreenActivity, MainActivity::class.java))
                finish()
            }

        })

    }
}