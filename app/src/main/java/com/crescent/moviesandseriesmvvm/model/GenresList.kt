package com.crescent.moviesandseriesmvvm.model

data class GenresList(
    val id : Int,
    val name : String
)