package com.crescent.moviesandseriesmvvm.model

import com.google.gson.annotations.SerializedName

data class MovieDetail(

    val adult : Boolean,
    val genres : List<GenresList>,
    val backdrop_path : String,
    val original_title : String,
    val overview : String,
    val poster_path : String,
    val release_date : String,
    val title : String,
    val status : String,
    val production_companies : List<ProductionCompanies>,
    val vote_average : Double
)