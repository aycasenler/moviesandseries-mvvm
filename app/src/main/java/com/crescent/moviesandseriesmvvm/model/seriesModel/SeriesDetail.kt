package com.crescent.moviesandseriesmvvm.model.seriesModel

import com.crescent.moviesandseriesmvvm.model.GenresList
import com.crescent.moviesandseriesmvvm.model.ProductionCompanies


data class SeriesDetail(
    val backdrop_path: String,
    val first_air_date: String,
    val genres : List<GenresList>,
    val homepage: String,
    val id: Int,
    val last_air_date: String,
    val number_of_seasons: Int,
    val number_of_episodes: Int,
    val name: String,
    val production_companies : List<ProductionCompanies>,
    val overview: String,
    val poster_path: String,
    val status: String,
    val vote_average: Double,
    val original_name: String
)