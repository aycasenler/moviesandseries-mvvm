package com.crescent.moviesandseriesmvvm.model

data class MoviesData(
    val results : List<MoviesProperties>
)