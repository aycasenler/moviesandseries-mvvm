package com.crescent.moviesandseriesmvvm.model.seriesModel

data class SeriesData(

    val results :List<SeriesProperties>
)