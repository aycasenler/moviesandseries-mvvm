package com.crescent.moviesandseriesmvvm.model.seriesModel

data class SeriesProperties(

    val name: String,
    var poster_path: String,
    val backdrop_path: String,
    val original_name: String,
    val overview: String,
    val first_air_date: String,
    val id : Int,
    val vote_average: Double
)