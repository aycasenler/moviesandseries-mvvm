package com.crescent.moviesandseriesmvvm.repository

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.crescent.moviesandseriesmvvm.api.ApiClient
import com.crescent.moviesandseriesmvvm.model.MovieDetail
import com.crescent.moviesandseriesmvvm.model.seriesModel.SeriesDetail
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SeriesDetailFragmentRepository  (val application: Application){
    val showProgress = MutableLiveData<Boolean>()
    val seriesTitle = MutableLiveData<String>()
    val seriesOriginalTitle = MutableLiveData<String>()
    val seriesOverview = MutableLiveData<String>()
    val seriesFirstAirDate = MutableLiveData<String>()
    val seriesVoteAverage = MutableLiveData<String>()
    val seriesPosterUrl = MutableLiveData<String>()
    val seriesBackdropUrl = MutableLiveData<String>()
    val seriesGenres = MutableLiveData<String>()
    val seriesProductionCompanies = MutableLiveData<String>()
    val genres = MutableLiveData<String>()
    val productionCompanies = MutableLiveData<String>()
    val seriesEpisodes = MutableLiveData<String>()
    val seriesSeasons = MutableLiveData<String>()
    var lastRequestTime : Long = -1

    fun getSeriesDetail(seriesId: Int ){

        if((System.currentTimeMillis() - lastRequestTime) < 10000){
            return
        }
        showProgress.value = true
        genres.value = ""
        productionCompanies.value = ""

        val call: Call<SeriesDetail> = ApiClient.getClient.getSeriesDetail(seriesId)
        call.enqueue(object : Callback<SeriesDetail> {
            override fun onFailure(call: Call<SeriesDetail>?, t: Throwable?) {
                showProgress.value= false
                Toast.makeText(application, "Error wile accessing the API", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<SeriesDetail>?,
                response: Response<SeriesDetail>?
            ) =
                if (response!!.isSuccessful) {
                    lastRequestTime = System.currentTimeMillis()
                    seriesTitle.value = response.body()!!.name
                    seriesOriginalTitle.value = response.body()!!.original_name
                    seriesOverview.value = response.body()!!.overview
                    seriesFirstAirDate.value = response.body()!!.first_air_date
                    seriesVoteAverage.value = response.body()!!.vote_average.toString()
                    seriesEpisodes.value = response.body()!!.number_of_episodes.toString()
                    seriesSeasons.value = response.body()!!.number_of_seasons.toString()
                    seriesPosterUrl.value =  "https://image.tmdb.org/t/p/w500" + response.body()!!.poster_path
                    seriesBackdropUrl.value ="https://image.tmdb.org/t/p/w500" + response.body()!!.backdrop_path

                    for (i in response.body()!!.genres) {
                        if (genres.value!!.isNotEmpty())
                            genres.value += ", " + i.name
                        else
                            genres.value += i.name
                    }

                    seriesGenres.value = genres.value

                    for (i in response.body()!!.production_companies) {
                        if (productionCompanies.value!!.isNotEmpty())
                            productionCompanies.value += ", " + i.name
                        else
                            productionCompanies.value += i.name
                    }

                    seriesProductionCompanies.value = productionCompanies.value

                    showProgress.value = false

                } else {
                    Toast.makeText(application, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }
}