package com.crescent.moviesandseriesmvvm.repository

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.crescent.moviesandseriesmvvm.api.ApiClient
import com.crescent.moviesandseriesmvvm.model.MovieDetail
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieDetailFragmentRepository (val application: Application){
    val showProgress = MutableLiveData<Boolean>()
    val movieTitle = MutableLiveData<String>()
    val movieOriginalTitle = MutableLiveData<String>()
    val movieOverview = MutableLiveData<String>()
    val movieReleaseDate = MutableLiveData<String>()
    val movieVoteAverage = MutableLiveData<String>()
    val moviePosterUrl = MutableLiveData<String>()
    val movieBackdropUrl = MutableLiveData<String>()
    val movieGenres = MutableLiveData<String>()
    val movieProductionCompanies = MutableLiveData<String>()
    val genres = MutableLiveData<String>()
    val productionCompanies = MutableLiveData<String>()


    var lastRequestTime : Long = -1

    fun getMovieDetail(movieId: Int ){

        if((System.currentTimeMillis() - lastRequestTime) < 10000){
            return
        }
        showProgress.value = true
        genres.value = ""
        productionCompanies.value = ""

        val call: Call<MovieDetail> = ApiClient.getClient.getMovieDetail(movieId)
        call.enqueue(object : Callback<MovieDetail> {
            override fun onFailure(call: Call<MovieDetail>?, t: Throwable?) {
                showProgress.value= false
                Toast.makeText(application, "Error wile accessing the API", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<MovieDetail>?,
                response: Response<MovieDetail>?
            ) =
                if (response!!.isSuccessful) {
                    lastRequestTime = System.currentTimeMillis()
                    movieTitle.value = response.body()!!.title
                    movieOriginalTitle.value = response.body()!!.original_title
                    movieOverview.value = response.body()!!.overview
                    movieReleaseDate.value = response.body()!!.release_date
                    movieVoteAverage.value = response.body()!!.vote_average.toString()
                    moviePosterUrl.value =  "https://image.tmdb.org/t/p/w500" + response.body()!!.poster_path
                    movieBackdropUrl.value =  "https://image.tmdb.org/t/p/w500" + response.body()!!.backdrop_path

                    for (i in response.body()!!.genres) {
                        if (genres.value!!.isNotEmpty())
                            genres.value += ", " + i.name
                        else
                            genres.value += i.name
                    }

                    movieGenres.value = genres.value

                    for (i in response.body()!!.production_companies) {
                        if (productionCompanies.value!!.isNotEmpty())
                            productionCompanies.value += ", " + i.name
                        else
                            productionCompanies.value += i.name
                    }

                    movieProductionCompanies.value = productionCompanies.value

                    showProgress.value = false

                } else {
                    Toast.makeText(application, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }
}