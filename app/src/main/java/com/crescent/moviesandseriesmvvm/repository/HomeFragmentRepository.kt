package com.crescent.moviesandseriesmvvm.repository

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.crescent.moviesandseriesmvvm.api.ApiClient
import com.crescent.moviesandseriesmvvm.model.MoviesData
import com.crescent.moviesandseriesmvvm.model.MoviesProperties
import com.crescent.moviesandseriesmvvm.model.seriesModel.SeriesData
import com.crescent.moviesandseriesmvvm.model.seriesModel.SeriesProperties
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeFragmentRepository(val application: Application) {
    val moviesList = MutableLiveData<List<MoviesProperties>>()
    val seriesList = MutableLiveData<List<SeriesProperties>>()
    val showProgress = MutableLiveData<Boolean>()


    fun getMovies(size: Int) {
        showProgress.value = true

        val call: Call<MoviesData> = ApiClient.getClient.getMovies()
        call.enqueue(object : Callback<MoviesData> {
            override fun onFailure(call: Call<MoviesData>?, t: Throwable?) {
                showProgress.value = false
                Toast.makeText(application, "Error wile accessing the API", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<MoviesData>?,
                response: Response<MoviesData>?
            ) =
                if (response!!.isSuccessful) {
                    moviesList.value = response.body()!!.results
                    moviesList.value =  moviesList.value!!.take(size)

                    showProgress.value = false

                } else {
                    Toast.makeText(application, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    fun getSeries(size: Int) {
        showProgress.value = true

        val call: Call<SeriesData> = ApiClient.getClient.getSeries()
        call.enqueue(object : Callback<SeriesData> {
            override fun onFailure(call: Call<SeriesData>?, t: Throwable?) {
                showProgress.value = false
                Toast.makeText(application, "Error wile accessing the API", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<SeriesData>?,
                response: Response<SeriesData>?
            ) =
                if (response!!.isSuccessful) {
                    seriesList.value = response.body()!!.results
                    seriesList.value =  seriesList.value!!.take(size)
                    showProgress.value = false

                } else {
                    Toast.makeText(application, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }
}