package com.crescent.moviesandseriesmvvm.api

import com.crescent.moviesandseriesmvvm.model.MovieDetail
import com.crescent.moviesandseriesmvvm.model.MoviesData
import com.crescent.moviesandseriesmvvm.model.MoviesProperties
import com.crescent.moviesandseriesmvvm.model.seriesModel.SeriesData
import com.crescent.moviesandseriesmvvm.model.seriesModel.SeriesDetail
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {
    @GET("movie/popular?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US&page=1")
    fun getMovies(): Call<MoviesData>

    @GET("movie/{movie_id}?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US")
    fun getMovieDetail(@Path("movie_id") movieId: Int): Call<MovieDetail>

    @GET("tv/popular?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US&page=1")
    fun getSeries(): Call<SeriesData>

    @GET("tv/{tv_id}?api_key=36ddc17ac8efd52e16e783c51ef16255&language=en-US")
    fun getSeriesDetail(@Path("tv_id") seriesId: Int): Call<SeriesDetail>
}